Lista 3 de Aprendizagem de Máquinas
===================================

Essa é a minha resolução das questões da terceira lista da disciplina de Aprendizagem de Máquinas. O relatório se encontra em `relatorio/relatorio.pdf`. E os códigos fontes se encontram em `src/`. A base "Concrete" da questão 2 está em `bases/`, e as imagens das letras da questão 1 estão em `letras/`.

## Instruções

Todos os algoritmos foram desenvolvidos para o Python 3. Apesar de eu ter tido os cuidados para que o código funcionasse também no Python 2, mas para o Python 2 eu não testei.
Também é importante instalar as seguintes dependências:
 - `numpy`
 - `matplotlib`
 - `scikit-learn`

No Ubuntu e no Windows, você pode instalar facilmente os pré-requisitos ao instalar a distribuição [Anaconda](https://www.continuum.io/downloads). Alternativamente, se você já tiver o Python instalado, você deveria ser capaz de instalar as bibliotecas usando o comando `pip install numpy matplotlib scikit-learn`.

## Questões

Eu organizei a estrutura dos scripts de forma que a resolução da questão *X* está dentro do diretório `src/questao-X/`. E criei no `Makefile` uma espécie de índice de como executar cada questão. Se você quiser executar a questão *1*, execute `make q1`. Se quiser executar a questão *7*, execute `make q7`.

## Aluno
 - Lucas Caetano Possatti
