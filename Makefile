#!/usr/bin/make -f
SHELL=/bin/bash
.ONESHELL:
.PHONY: all clean q1 q2 q3


REPORT_PDF = ./relatorio/relatorio.pdf
REPORT_SOURCES = ./relatorio/relatorio.tex
SOURCE_LIST = $(shell find src/ -regex ".*\.\(py\|out\)")
BASE_FILES = $(shell find bases/ -regex ".*\.\(csv\|txt\)")
ZIP_SOURCES = $(REPORT_PDF) $(SOURCE_LIST) $(BASE_FILES) letras README.md Makefile
ZIP_PACKAGE = am-lista3-lucas-possatti.zip


all: $(ZIP_PACKAGE)

$(ZIP_PACKAGE): $(ZIP_SOURCES)
	zip -r $(@) $(?)

$(REPORT_PDF): $(REPORT_SOURCES)
	$(MAKE) --directory relatorio

clean:
	rm -f $(ZIP_PACKAGE)


## How to run each question ##
q1:
	python src/questao-1/main.py
q2:
	python src/questao-2/main.py
q3:
	python src/questao-3/main.py
