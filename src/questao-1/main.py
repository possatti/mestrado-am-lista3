#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from collections import OrderedDict

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)
print('Numpy version:', np.version.version, file=sys.stderr)

def f(v):
    return 1 if v >= 0 else -1

def main():
    np.random.seed(7)
    letras = OrderedDict()
    letras['B'] = plt.imread(os.path.join(args.letras, 'B.bmp'))
    letras['U'] = plt.imread(os.path.join(args.letras, 'U.bmp'))
    letras['L'] = plt.imread(os.path.join(args.letras, 'L.bmp'))
    letras['P'] = plt.imread(os.path.join(args.letras, 'P.bmp'))
    for letra, img in letras.items():
        letras[letra] = img[:,:,0]
        letras[letra] = np.where(letras[letra] > 0, 1, -1)
    assert letras['B'].size == letras['L'].size and \
           letras['P'].size == letras['L'].size and \
           letras['U'].size == letras['L'].size

    size = letras['L'].size
    X = np.vstack([ img.reshape(size) for letra, img in letras.items() ])
    C = np.array(list(letras.keys()))

    # Training.
    N = 2
    d = letras['L'].size
    n_neurons = d
    W = np.zeros(shape=(n_neurons, n_neurons))
    for i in range(n_neurons):
        for j in range(n_neurons):
            if i != j:
                ms = range(N)
                W[i,j] = np.dot(X[ms,i], X[ms,j])

    # Testing.
    predictions = []
    for x, c in zip(X[2:],C[2:]):
        t = 0
        Y = [x]
        while True:
            y = np.empty(shape=d)
            for i in range(d):
                the_sum = 0
                for j in range(d):
                    the_sum += W[i,j]*Y[t][j]
                y[i] = f(the_sum)
            if (Y[t] == y).all():
                break
            Y.append(y)
            t += 1
        for n in range(N):
            if (Y[-1] == X[n]).all():
                predictions.append(C[n])

    print('Predictions for L and P:', predictions)

if __name__ == '__main__':
    # Arguments and options.
    default_letras_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'letras'))
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--letras', default=default_letras_dir)
    args = parser.parse_args()
    main()
