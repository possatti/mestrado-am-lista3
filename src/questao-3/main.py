#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)

kNN = np.array([
    66.40,
    65.63,
    56.95,
    64.67,
    68.72,
    70.37,
    61.87,
    61.02,
    79.64,
    51.13,
    76.56,
    77.40,
    73.89,
    52.96,
    57.86,
    60.06,
    70.39,
    54.10,
    71.64,
    53.20,
    69.61,
    64.83,
    73.37,
    71.45,
])

kNNpp = np.array([
    84.10,
    61.81,
    71.98,
    75.81,
    72.50,
    79.71,
    78.84,
    68.76,
    72.95,
    60.46,
    89.52,
    65.02,
    63.19,
    71.17,
    65.94,
    74.69,
    70.18,
    88.55,
    87.61,
    61.58,
    82.14,
    68.07,
    72.69,
    76.44,
])

def main():
    assert kNN.size == kNNpp.size

    # Teste de Wilcoxon
    N = kNN.size
    print('N:', N, file=sys.stderr)
    d = kNN - kNNpp
    ordering = np.argsort(np.fabs(d))
    ranks = np.arange(N) + 1
    Rp = ranks[d[ordering] > 0].sum()
    Rm = ranks[d[ordering] < 0].sum()
    T = min(Rp, Rm)
    print('Rp, Rm:', Rp, Rm, file=sys.stderr)

    if T <= 81:
        if Rp > Rm:
            print('kNN é superior ao kNN++. Hipótese nula rejeitada pelo teste de Wilcoxon.')
        else:
            print('kNN++ é superior ao kNN. Hipótese nula rejeitada pelo teste de Wilcoxon.')
    else:
        print('A hipótese nula NÃO foi rejeitada pelo teste de Wilcoxon.')


    # Teste de sinal
    kNN_wins = (d > 0).sum()
    kNNpp_wins = (d < 0).sum()
    print('kNN_wins, kNNpp_wins:', kNN_wins, kNNpp_wins, file=sys.stderr)
    if max(kNN_wins, kNNpp_wins) >= 18:
        if kNN_wins > kNNpp_wins:
            print('kNN é superior ao kNN++. Hipótese nula rejeitada pelo teste do sinal.')
        else:
            print('kNN++ é superior ao kNN. Hipótese nula rejeitada pelo teste do sinal.')
    else:
        print('A hipótese nula NÃO foi rejeitada segundo o teste do sinal.')

if __name__ == '__main__':
    main()
