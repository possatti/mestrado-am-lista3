#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.dummy import DummyRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, GridSearchCV

import numpy as np
import argparse
import math
import sys
import re
import os

print(sys.version, file=sys.stderr)

class GRNN(BaseEstimator, RegressorMixin):
    def __init__(self, sigma=0.1):
        self.sigma = sigma

    def fit(self, X, y):
        assert len(X) == len(y)
        assert X.ndim == 2

        # Keep in mind that for the Ws, the rows correspond to samples,
        # and columns to attributes.
        n_samples, n_attr = X.shape
        self.patterns_W = np.empty(shape=(n_samples, n_attr))
        for j in range(n_samples):
            self.patterns_W[j,:] = X[j]

        # Prepare sum layer (numerator, denominator).
        self.sum_W = np.empty(shape=(n_samples, 2))
        self.sum_W[:,0] = y # Numerator weights.
        self.sum_W[:,1] = 1 # Denominator weights.

        return self

    def predict(self, X):
        def f(x, w_j, sigma):
            return math.exp( ( np.dot(-(x-w_j)[np.newaxis,:], (x-w_j)[:,np.newaxis]) )/(2*sigma**2) )

        predictions = []
        for x in X:
            numerator = 0
            denominator = 0
            for j in range(self.patterns_W.shape[0]):
                numerator += self.sum_W[j,0] * f(x, self.patterns_W[j], self.sigma)
                denominator += self.sum_W[j,1] * f(x, self.patterns_W[j], self.sigma)
            y = numerator / denominator
            predictions.append(y)
        return np.array(predictions)

sigma_search_params = {
    # 'sigma': np.arange(10)/10+0.1 # [0.1, 0.2, ..., 1]
    # 'sigma': np.arange(5)/5+0.2 # [0.2, 0.4, ..., 1]
    'sigma': [0.1, 0.3, 0.6, 0.9, 1]
    # 'sigma': [0.1, 0.5]
}

def main():
    np.random.seed(7)
    dataset = np.loadtxt(args.concrete_dataset, dtype=float, skiprows=1, delimiter=',')

    # Preprocess the data.
    scaler = StandardScaler()
    dataset = scaler.fit_transform(dataset)

    N = 4
    for n in range(N):
        print('Iteração:', n)
        np.random.shuffle(dataset)
        data = dataset[:,:-1]
        labels = dataset[:,-1]
        train_data, test_data, train_labels, test_labels = train_test_split(data, labels, test_size=0.25)
        print('Training and testing using our model...', file=sys.stderr)
        grnn = GRNN()
        grid = GridSearchCV(grnn, sigma_search_params, n_jobs=-1, verbose=1, cv=3)
        grid.fit(train_data, train_labels)
        grnn_r2 = grid.score(test_data, test_labels)
        print('Best parameters:', grid.best_params_)
        print('GRNN R2:', grnn_r2)

        print('Training and testing using a dummy regressor...', file=sys.stderr)
        dummy = DummyRegressor()
        dummy.fit(train_data, train_labels)
        dummy_r2 = dummy.score(test_data, test_labels)
        print('Dummy R2:', dummy_r2)
        print()

if __name__ == '__main__':
    # Arguments and options
    default_concrete_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'bases', 'Concrete_Data.csv'))
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--concrete-dataset', metavar='CSV_PATH', default=default_concrete_dataset_path, help='Path to the concrete dataset. It should be a CSV.')
    args = parser.parse_args()
    main()
